import random

# initialise players scores to zero

player1_score = 0
player2_score = 0

# repeat evrything in this block 10 times
for i in range(10):

    #generate random number between 1 and 6 for each player
    player1_value = random.randint(1, 6)
    player2_value = random.randint(1, 6)

    #Display the values

    print("Player 1 rolled: ",player1_value)
    print("Player 2 rolled:", player2_value)

    if player1_value > player2_value:
        print("player 1 wins")
        player1_score = player1_score + 1
    elif player2_value > player1_value:
        print("player 2 wins")

        player2_score = player2_score + 1

    else:
        print("it's a draw")


        input("press enter to continue")

    print(" game over")
    print("player 1 score:", player1_score)
    print("player 2 score:", player2_score)
